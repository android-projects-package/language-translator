package com.textTranslation.assignment.Services;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.textTranslation.assignment.Model.Language;
import com.textTranslation.assignment.Model.Phrase;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {


    private List<String> phrasesList;


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "contactsManager";
    private static final String TABLE_PHRASE = "phrases";
    private static final String TABLE_LANGUAGES = "languages";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_NUMBER = "number";


    public DatabaseHandler(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_PHRASE_TABLE = "CREATE TABLE " + TABLE_PHRASE + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT " + ")";

        String CREATE_LANGUAGES_TABLE = "CREATE TABLE " + TABLE_LANGUAGES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NUMBER + " INTEGER " + " )";

        db.execSQL(CREATE_PHRASE_TABLE);
        db.execSQL(CREATE_LANGUAGES_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHRASE);

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_LANGUAGES);

        // Create tables again

        onCreate(db);

    }

    // code to add the new contact
    public void addPhrase(Phrase phrase) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, phrase.getName()); // Contact Name

        // Inserting Row

        db.insert(TABLE_PHRASE, null, values);

        //2nd argument is String containing nullColumnHack

        db.close(); // Closing database connection

    }

    public void addLanguage(Language language){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NUMBER,language.getNumber());
        sqLiteDatabase.insert(TABLE_LANGUAGES,null,values);
        sqLiteDatabase.close();
    }


/*    // code to get the single contact
    Contact getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        return contact;
    }*/

    // code to get all contacts in a list view
    public List<Phrase> getAllPhrases() {
        List<Phrase> phrases = new ArrayList<Phrase>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PHRASE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Phrase phrase = new Phrase();
                phrase.setId(Integer.parseInt(cursor.getString(0)));
                phrase.setName(cursor.getString(1));

                // Adding contact to list
                phrases.add(phrase);
            } while (cursor.moveToNext());
        }
        // return contact list
        return phrases;

    }

    // this method is used to get all the subscribed languages from the db
    public List<Language> getSubscribedLanguages(){

        List<Language> languages = new ArrayList<Language>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LANGUAGES;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Language language = new Language();
                language.setId(Integer.parseInt(cursor.getString(0)));
                language.setNumber(Integer.parseInt(cursor.getString(1)));
                // Adding contact to list
                languages.add(language);
            } while (cursor.moveToNext());
        }
        // return contact list
        return languages;
    }

    // to delete all the subscribed languages from database
    public void deleteAllLanguages(){
        SQLiteDatabase sqLiteDatabas = this.getWritableDatabase();
        sqLiteDatabas.delete(TABLE_LANGUAGES, null, null);
    }

    // code to update the single contact

    public void updatePhrase(Phrase phrase) {

        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "update phrases set name = '"+phrase.getName()+"' where id = '"+phrase.getId()+"' ";

        db.execSQL(sql);

        /*ContentValues values = new ContentValues();
        values.put(KEY_NAME, phrase.getName());
        // updating row

        return db.update(TABLE_PHRASE, values, KEY_ID + " = ?",
                new String[] { String.valueOf(phrase.getId()) });*/

    }

/*    // Deleting single contact
    public void deleteContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
        db.close();
    }*/

/*
    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }
*/



    public List<String> getPhrasesList(List<Phrase> phrases){

        List<String> list = new ArrayList<>();

        for(Phrase phrase: phrases){
            list.add(phrase.getName());
        }


        return list;
    }

}
