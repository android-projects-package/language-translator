package com.textTranslation.assignment.Activities;
import androidx.appcompat.app.AppCompatActivity;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.textTranslation.assignment.Model.Language;
import com.textTranslation.assignment.R;
import com.textTranslation.assignment.Services.DatabaseHandler;
import java.util.ArrayList;
import java.util.List;

public class LanguageSubscription extends AppCompatActivity implements
        View.OnClickListener {

    Button button;

    ListView listView;

    ArrayAdapter<String> adapter;

    // array of languages to show on to the listview
    String translationLanguages[] = {"Urdu","Dutch","French","Russian","Persian","Spanish"
    ,"Chinese","Hindi","Thai","Turkish","Telugu","Bengali","Irish","Indonesian","Malyalam"};

    DatabaseHandler databaseHandler;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_subscription);

        findViewsById();

        databaseHandler = new DatabaseHandler(this);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, translationLanguages);

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        listView.setAdapter(adapter);

        /* get the list of subscribed languages from the sqlite database.
        initially the list is empty and when user subscribes the languages they will be
        retrieved here from the database next time.
         */
        List<Language> languageList = databaseHandler.getSubscribedLanguages();

        if(languageList.size() == 0){
            Toast.makeText(this, "No Language Subscribed", Toast.LENGTH_SHORT).show();
        }
        else{

            // if the list of language is not null then set the subscribed languages on listview
            for(Language language:languageList){

                listView.setItemChecked(language.getNumber(),true);
            }

        }



        button.setOnClickListener(this);
    }

    private void findViewsById() {
        listView = (ListView) findViewById(R.id.list);
        button = (Button) findViewById(R.id.testbutton);
    }

    public void onClick(View v) {

        if(listView.getCheckedItemCount() == 0){

            Toast.makeText(this, "Please select atleast one language", Toast.LENGTH_SHORT).show();
            return;

        }

        // to get the checked values of the list
        SparseBooleanArray checked = listView.getCheckedItemPositions();

        for(int i=0;i<checked.size();i++){

            Log.d("Checked Values = ",checked.keyAt(i)+"");

        }

        ArrayList<String> selectedItems = new ArrayList<String>();

        for (int i = 0; i < checked.size(); i++) {

            // Item position in adapter
            int position = checked.keyAt(i);

            if (checked.valueAt(i))
                selectedItems.add(adapter.getItem(position));

        }



        // first delete all languages

        databaseHandler.deleteAllLanguages();

        // after deletion store the newly subscribed languages....

        for (int i = 0; i < selectedItems.size(); i++) {
            int number = checked.keyAt(i);
            databaseHandler.addLanguage(new Language(number));
        }

        Toast.makeText(this, "Subscribed Successully!", Toast.LENGTH_SHORT).show();

        finish();
    }

}