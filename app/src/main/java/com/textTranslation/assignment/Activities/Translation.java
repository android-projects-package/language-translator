package com.textTranslation.assignment.Activities;
import androidx.appcompat.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import com.ibm.watson.developer_cloud.android.library.audio.StreamPlayer;
import com.ibm.watson.developer_cloud.language_translator.v3.LanguageTranslator;
import com.ibm.watson.developer_cloud.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.developer_cloud.language_translator.v3.model.TranslationResult;
import com.ibm.watson.developer_cloud.service.security.IamOptions;
import com.ibm.watson.developer_cloud.text_to_speech.v1.TextToSpeech;
import com.ibm.watson.developer_cloud.text_to_speech.v1.model.SynthesizeOptions;
import com.textTranslation.assignment.Model.Language;
import com.textTranslation.assignment.R;
import com.textTranslation.assignment.Services.DatabaseHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class Translation extends AppCompatActivity {

    ListView listView;

    EditText translationEditText;

    Button translateButton,speakButton;

    Spinner languagesSpinner;

    DatabaseHandler databaseHandler;

    ArrayAdapter<String> languageAdapter;

    boolean isTranslated;

    private int currentPosition = -1;

    String translationLanguages[] = {"Urdu","Dutch","French","Russian","Persian","Spanish"
            ,"Chinese","Hindi","Thai","Turkish","Telugu","Bengali","Irish","Indonesian","Malyalam"};

    List<String> subscribedLanguages;

    String phraseToBeTranslated;

    ProgressDialog progressDialog;

    private LanguageTranslator translationService;

    private String firstTranslation = "";

    private String selectedTargetLanguage = "nl";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation);

        listView = findViewById(R.id.list_view);

        translationEditText = findViewById(R.id.translateEditText);

        translateButton = findViewById(R.id.translateButton);

        speakButton = findViewById(R.id.speakButton);

        languagesSpinner = findViewById(R.id.spinner);

        databaseHandler = new DatabaseHandler(this);

        progressDialog = new ProgressDialog(this);

        progressDialog.setTitle("Translation");

        progressDialog.setMessage("Translating your phrase.....");

        progressDialog.setCancelable(false);

        subscribedLanguages = new ArrayList<>();

        subscribedLanguages.add("---Select Language---");

        List<Language> languageList = databaseHandler.getSubscribedLanguages();

        for(Language language:languageList){
            subscribedLanguages.add(translationLanguages[language.getNumber()]);
        }

        languageAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, subscribedLanguages);

        languagesSpinner.setAdapter(languageAdapter);

        List<String> phrases = databaseHandler.getPhrasesList(databaseHandler.getAllPhrases());

        Collections.sort(phrases);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_single_choice, android.R.id.text1, phrases);
        listView.setChoiceMode(listView.CHOICE_MODE_SINGLE);
        listView.setAdapter(adapter);


        translationService = initLanguageTranslatorService();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                // TODO Auto-generated method stub

                currentPosition = position;

                phraseToBeTranslated = adapter.getItem(position);

            }
        });

        languagesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedTargetLanguage = getLanguageCode(languagesSpinner.getItemAtPosition(position)+"");
              //  Toast.makeText(Translation.this, "Selected Language = "+languagesSpinner.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
              //  Toast.makeText(Translation.this, "Language Code = "+selectedTargetLanguage, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        translateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(currentPosition == -1){
                    Toast.makeText(Translation.this, "Select phrase to translate", Toast.LENGTH_SHORT).show();
                }
                else if(languagesSpinner.getSelectedItemPosition() == 0){
                    Toast.makeText(Translation.this, "Select language to translate", Toast.LENGTH_SHORT).show();
                }
                else{
                    progressDialog.show();
                    new TranslationTask().execute(phraseToBeTranslated);
                }
            }
        });

        speakButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isTranslated){
                    // do speak
                    speakup();
                }
                else{
                    Toast.makeText(Translation.this, "First translate the language", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    //Translate the text entered in the language selected
    private class TranslationTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            try{
                TranslateOptions translateOptions = new TranslateOptions.Builder()
                        .addText(params[0])
                        .source("en")
                        .target(selectedTargetLanguage)
                        .build();


                TranslationResult result = translationService.translate(translateOptions).execute();
                firstTranslation = result.getTranslations().get(0).getTranslationOutput();
                showTranslation(firstTranslation);
                // use firstTranslation to convert to speech

            }
            catch(Exception exception){
                Toast.makeText(Translation.this, "Exception: "+exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return "Did translate";

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            isTranslated = true;
            Toast.makeText(Translation.this, "Translation Successfull", Toast.LENGTH_SHORT).show();
        }
    }


    //Use IBM Watson Translator to translate the provided text
    private LanguageTranslator initLanguageTranslatorService() {
        IamOptions options = new IamOptions.Builder()
                .apiKey(getResources().getString(R.string.language_translator_apikey))  //Add your api key here
                .build();

        LanguageTranslator service = new LanguageTranslator("2018-05-01", options);

        service.setEndPoint(getResources().getString(R.string.language_translator_url));  //Add your url here
        return service;
    }


    //Show the translated text in the text view field.
    private void showTranslation(final String translation) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                translationEditText.setText(translation);
            }
        });
    }


    public String getLanguageCode(String language){
        String languageCode = null;
        if(language.equals("Urdu")){
            languageCode = "ur";
            speakLanguage = null;
        }
        else if(language.equals("Dutch")){
            languageCode = "nl";
            speakLanguage = "nl-NL_EmmaVoice";
        }
        else if(language.equals("French")){
            languageCode = "fr";
            speakLanguage = SynthesizeOptions.Voice.FR_FR_RENEEVOICE;
        }
        else if(language.equals("Russian")){
            languageCode = "ru";
            speakLanguage = null;
        }
        else if(language.equals("Persian")){
            languageCode = "fa";
            speakLanguage = null;
        }
        else if(language.equals("Spanish")){
            languageCode = "es";
            speakLanguage = "es-ES_EnriqueVoice";
        }
        else if(language.equals("Chinese")){
            languageCode = "zh";
            speakLanguage = "zh-CN_LiNaVoice";
        }
        else if(language.equals("Hindi")){
            languageCode = "hi";
            speakLanguage = null;
        }
        else if(language.equals("Thai")){
            languageCode = "th";
            speakLanguage = null;
        }
        else if(language.equals("Turkish")){
            languageCode = "tr";
            speakLanguage = null;
        }
        else if(language.equals("Bengali")){
            languageCode = "bn";
            speakLanguage = null;
        }
        else if(language.equals("Irish")){
            languageCode = "ga";
            speakLanguage = null;
        }
        else if(language.equals("Indonesian")){
            languageCode = "id";
            speakLanguage = null;
        }
        else if(language.equals("Malayalam")){
            languageCode = "ml";
            speakLanguage = null;
        }

        return languageCode;
    }



    private TextToSpeech textToSpeech;


    //Use IBM Text-to-Speech to speak the translated text.
    public void speakup() {
        IamOptions options = new IamOptions.Builder()
                .apiKey(getResources().getString(R.string.text_speech_apikey))   //Add your api key here
                .build();
        textToSpeech = new TextToSpeech(options);
        textToSpeech.setEndPoint(getResources().getString(R.string.text_speech_url));   //Add your url here
        //If no text is entered to translate, prompt the user

        if (firstTranslation.equals("")) {
            Toast.makeText(getApplicationContext(), "Please translate first", Toast.LENGTH_SHORT).show();
        } else {

            if(speakLanguage == null){
                Toast.makeText(this, "Looks like this language is not supported.", Toast.LENGTH_SHORT).show();
            }

            else{
                    progressDialog.setTitle("Speak");
                    progressDialog.setMessage("Please wait.....");
                    progressDialog.show();
                    new SynthesisTask().execute(firstTranslation);

            }
        }
    }




    private StreamPlayer player = new StreamPlayer();
    private String speakLanguage = "";
    //Play the audio for translated word in the language selected
    private class SynthesisTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            SynthesizeOptions synthesizeOptions = new SynthesizeOptions.Builder()
                    .text(params[0])
                    .voice(speakLanguage)
                    .accept(SynthesizeOptions.Accept.AUDIO_WAV)
                    .build();
            player.playStream(textToSpeech.synthesize(synthesizeOptions).execute());
            return "Did synthesize";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
        }
    }





}
