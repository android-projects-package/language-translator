package com.textTranslation.assignment.Activities;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.textTranslation.assignment.Model.Phrase;
import com.textTranslation.assignment.R;
import com.textTranslation.assignment.Services.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

/*
Author Name: Moiz Ahmad Mughal
Author Email: moizahmad007@gmail.com
*/

public class EditPhrases extends AppCompatActivity {

    ListView listView;

    ArrayAdapter adapter;

    List<String> phrasesList = new ArrayList<>();

    DatabaseHandler databaseHandler;

    EditText editTextField;

    Button editButton,saveButton;

    // indicates that nothing is selected

    private boolean isItemSelected = false;

    private int currentPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_phrases);






        editTextField = findViewById(R.id.editTextBox);

        editTextField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(! editTextField.getText().toString().equals("")){
                    // this mean we need to show another dialog.
                }
            }
        });

        saveButton = findViewById(R.id.saveButton);

        editButton = findViewById(R.id.editButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentPosition = currentPosition + 1;

                updatePhrase(currentPosition);

                phrasesList = databaseHandler.getPhrasesList(databaseHandler.getAllPhrases());

                listView.setChoiceMode(listView.CHOICE_MODE_SINGLE);

                adapter = new ArrayAdapter(EditPhrases.this,android.R.layout.simple_list_item_single_choice,phrasesList);

                listView.setAdapter(adapter);

                editTextField.setText("");

                isItemSelected = false;
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(currentPosition == -1){
                    // means nothing is selected at this stage....
                    Toast.makeText(EditPhrases.this, "Select a phrase to edit", Toast.LENGTH_SHORT).show();
                }
                else {
                    editTextField.setText(phrasesList.get(currentPosition));
                }
            }
        });

        listView = findViewById(R.id.list_view);

        databaseHandler = new DatabaseHandler(this);

        phrasesList = databaseHandler.getPhrasesList(databaseHandler.getAllPhrases());

        listView.setChoiceMode(listView.CHOICE_MODE_SINGLE);

        adapter = new ArrayAdapter(EditPhrases.this,android.R.layout.simple_list_item_single_choice,phrasesList);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                currentPosition = position;

                if(isItemSelected == false ){

                    isItemSelected = true;
                }
                else{

                    if(! editTextField.getText().toString().equals("")){
                        editTextField.setText(phrasesList.get(position));
                    }
                }
            }

        });

    }

    private void updatePhrase(int position) {

        Phrase phrase = new Phrase(position,editTextField.getText().toString());

        databaseHandler.updatePhrase(phrase);

        Toast.makeText(this, "Record Updated Successfully! ", Toast.LENGTH_SHORT).show();

    }



}

