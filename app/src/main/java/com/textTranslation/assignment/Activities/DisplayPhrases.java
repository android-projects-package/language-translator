package com.textTranslation.assignment.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.textTranslation.assignment.Model.Phrase;
import com.textTranslation.assignment.R;
import com.textTranslation.assignment.Services.DatabaseHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DisplayPhrases extends AppCompatActivity {

    ListView listView;

    TextView textView;

    List<Phrase> phrasesList = new ArrayList<>();

    List<String> phrases = new ArrayList<>();

    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_display_phrases);

        listView=(ListView)findViewById(R.id.listView);

        textView=(TextView)findViewById(R.id.textView);

        databaseHandler = new DatabaseHandler(this);

        getPhrases();

        // adapter to fill the listview with the phrases stored in database.
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, phrases);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                // TODO Auto-generated method stub
                String value=adapter.getItem(position);
                Toast.makeText(getApplicationContext(),value, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getPhrases() {

        phrasesList = databaseHandler.getAllPhrases();

        for(Phrase phrase: phrasesList){

            phrases.add(phrase.getName());

        }
        // to sort the phrases list in alphabetical order.
        Collections.sort(phrases);
    }
}
