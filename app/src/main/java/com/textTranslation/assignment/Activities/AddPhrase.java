package com.textTranslation.assignment.Activities;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.textTranslation.assignment.Model.Phrase;
import com.textTranslation.assignment.R;
import com.textTranslation.assignment.Services.DatabaseHandler;

import java.util.List;

public class AddPhrase extends AppCompatActivity {

    EditText addPhraseEditText;

    Button saveButton;

    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_phrase);

        addPhraseEditText = findViewById(R.id.addPhraseEditText);

        saveButton = findViewById(R.id.saveButton);

        db = new DatabaseHandler(this);




        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addPhraseEditText.getText().toString().equals("")){
                    Toast.makeText(AddPhrase.this, "Enter a phrase or word", Toast.LENGTH_SHORT).show();
                }
                else{
                    savePhrase();
                }
            }
        });
    }

    private void savePhrase() {
        db.addPhrase(new Phrase(addPhraseEditText.getText().toString()));
        Toast.makeText(this, "Phrase Added Successfully", Toast.LENGTH_SHORT).show();
        addPhraseEditText.setText("");
    }
}
