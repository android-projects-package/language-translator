package com.textTranslation.assignment.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.textTranslation.assignment.R;

public class MainActivity extends AppCompatActivity {

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.addPhrase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(getApplicationContext(),AddPhrase.class);
                startActivity(intent);

            }
        });

        findViewById(R.id.displayPhrases).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(getApplicationContext(),DisplayPhrases.class);
                startActivity(intent);

            }
        });

        findViewById(R.id.editPhrases).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(getApplicationContext(),EditPhrases.class);
                startActivity(intent);

            }
        });

        findViewById(R.id.languageSubscription).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(getApplicationContext(),LanguageSubscription.class);
                startActivity(intent);

            }
        });

        findViewById(R.id.translate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(getApplicationContext(),Translation.class);
                startActivity(intent);

            }
        });


    }
}
