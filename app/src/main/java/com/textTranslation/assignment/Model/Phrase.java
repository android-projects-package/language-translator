package com.textTranslation.assignment.Model;

public class Phrase {

    private Integer id;

    private String name;

    public Phrase(){

    }

    public Phrase(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Phrase(String name){
        this.name = name;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
