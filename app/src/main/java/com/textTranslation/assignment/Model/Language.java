package com.textTranslation.assignment.Model;

public class Language {
    private int id;
    private int number;

    public Language(){

    }

    public Language(int number){
        this.number = number;
    }

    public Language(int id, int number) {
        this.id = id;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
